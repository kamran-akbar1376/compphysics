import numpy as np
from scipy.integrate import simps


def func1(r):
    """
    :param r:
        real number which can be number, vector, or matrix
    :return:
        output is real number which can be number, vector, or matrix
    """
    return r ** 2 * np.exp(-2 * r)


def func2(x):
    """
    :param x:
        real number which can be number, vector, or matrix
    :return:
        output is real number which can be number, vector, or matrix
    """
    return np.sin(x) / x


def func3(x):
    """
    :param x:
        real number which can be number, vector, or matrix
    :return:
        output is real number which can be number, vector, or matrix
    """
    return np.exp(np.sin(x ** 3))


def func4(x, y):
    """
    :param x:
        real number which can be number, vector, or matrix
    :param y:
        real number which can be number, vector, or matrix
    :return:
        output is calculated for each x through all y
    """
    return x[:, None] * np.exp(-np.sqrt(x[:, None] ** 2 + y ** 2))


def func5(r, r_a, r_b):
    """
    :param r:
        points with x, y, z in 3D world coordinates
    :param r_a:
        starting point
    :param r_b:
        ending point
    :return:
        result integration
    """
    return np.abs(np.exp(-np.sqrt(np.sum((r - r_a)**2, axis=1))) / np.pi)**2 / np.sqrt(np.sum((r - r_b) ** 2, axis=1))


def integration():
    x = np.linspace(0, 4 * np.pi / 3, 10)
    y = func1(x)
    integral1 = simps(y, x)
    true_integral1 = 0.25
    print('Error1:', abs(integral1 - true_integral1))

    x = np.linspace(1e-6, 1, 10 ** 7)
    y = func2(x)
    integral2 = simps(y, x)
    true_integral2 = 0.95
    print('Error2:', abs(integral2 - true_integral2))

    x = np.linspace(0, 5, 10 ** 3)
    y = func3(x)
    integral3 = simps(y, x)
    true_integral3 = 6.65
    print('Error3:', abs(integral3 - true_integral3))

    # integration in 2 dimensions happens through integrating each dimension separately
    x = np.linspace(0, 2, 10)
    y = np.linspace(-2, 2, 10)
    z = func4(x, y)
    integral4 = simps(simps(z, x), y)
    true_integral4 = 1.57
    print('Error4:', abs(integral4 - true_integral4))

    # function's integration through a vector r shows a point in 3D coordinate
    r_a = np.array([1.0, 0.0, 0.0])
    r_b = np.array([2.0, 0.0, 0.0])
    R = np.sqrt(np.sum((r_a - r_b) ** 2))
    r = np.linspace(r_a, r_b - 1e-5, 10)
    j = func5(r, r_a, r_b)
    integral5 = simps(j, dx=np.max(r[0, :] - r[1, :]))
    true_integral5 = (1 - (1 + R) * np.exp(-2 * R)) / R
    print('Error5', abs(integral5 - true_integral5))
