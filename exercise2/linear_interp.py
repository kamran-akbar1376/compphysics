"""
Linear interpolation in 1d, 2d, and 3d

Intentionally unfinished :)

Related to FYS-4096 Computational Physics
exercise 2 assignments.

By Ilkka Kylanpaa on January 2019
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

"""
Add basis functions l1 and l2 here
"""


def l1(t):
    return 1 - t


def l2(t):
    return t


class linear_interp:
    """
        linear_interp class is for linear interpolation in 1 to 3 dimensions
    """

    def __init__(self, *args, **kwargs):
        """
        This constructor define the interpolation's dimension, and get each dimensions data through kwargs, and
        calculate grid space between each dimension's input.
        """
        self.dims = kwargs['dims']
        if (self.dims == 1):
            self.x = kwargs['x']
            self.f = kwargs['f']
            self.hx = np.diff(self.x)
        elif (self.dims == 2):
            self.x = kwargs['x']
            self.y = kwargs['y']
            self.f = kwargs['f']
            self.hx = np.diff(self.x)
            self.hy = np.diff(self.y)
        elif (self.dims == 3):
            self.x = kwargs['x']
            self.y = kwargs['y']
            self.z = kwargs['z']
            self.f = kwargs['f']
            self.hx = np.diff(self.x)
            self.hy = np.diff(self.y)
            self.hz = np.diff(self.z)
        else:
            print('Either dims is missing or specific dims is not available')

    def eval1d(self, x):
        """
        calculating the interpolation values in 1 dimension
        :param x:
            a query to evaluate the function's output by interpolating in 1 dimension
        :return:
            interpolated values in 1 dimension
        """
        if np.isscalar(x):
            x = np.array([x])
        N = len(self.x) - 1
        f = np.zeros((len(x),))
        ii = 0
        for val in x:
            # finding the last index which its value is smaller than val
            i = np.floor(np.where(self.x <= val)[0][-1]).astype(int)
            # interpolation process with boundary condition
            if i == N:
                f[ii] = self.f[i]
            else:
                # finding the interpolation ratio
                t = (val - self.x[i]) / self.hx[i]
                # calculate interpolated output equation 1 in second slides
                f[ii] = self.f[i] * l1(t) + self.f[i + 1] * l2(t)
            ii += 1
        return f

    def eval2d(self, x, y):
        """
        calculating the interpolation values in 2 dimension
        :param x:
            a query in x dimension to evaluate the function's output by interpolating in 2 dimension
        :param y:
            a query in y dimension to evaluate the function's output by interpolating in 2 dimension
        :return:
            interpolated values in 2 dimension
        """
        if np.isscalar(x):
            x = np.array([x])
        if np.isscalar(y):
            y = np.array([y])
        Nx = len(self.x) - 1
        Ny = len(self.y) - 1
        f = np.zeros((len(x), len(y)))
        A = np.zeros((2, 2))
        ii = 0
        for valx in x:
            # finding the last index which its value is smaller than val in x dimension
            i = np.floor(np.where(self.x <= valx)[0][-1]).astype(int)
            if (i == Nx):
                i -= 1
            jj = 0
            for valy in y:
                # finding the last index which its value is smaller than val in y dimension
                j = np.floor(np.where(self.y <= valy)[0][-1]).astype(int)
                if (j == Ny):
                    j -= 1
                # finding the interpolation ratio in x dimension
                tx = (valx - self.x[i]) / self.hx[i]
                # finding the interpolation ratio in y dimension
                ty = (valy - self.y[j]) / self.hy[j]
                # calculating the interpolated value in 2 dimensions by matrix multiplication equation 2 in second
                # slides
                ptx = np.array([l1(tx), l2(tx)])
                pty = np.array([l1(ty), l2(ty)])
                A[0, :] = np.array([self.f[i, j], self.f[i, j + 1]])
                A[1, :] = np.array([self.f[i + 1, j], self.f[i + 1, j + 1]])
                f[ii, jj] = np.dot(ptx, np.dot(A, pty))
                jj += 1
            ii += 1
        return f

    # end eval2d

    def eval3d(self, x, y, z):
        """
        calculating the interpolation values in 3 dimension
        :param x:
            a query in x dimension to evaluate the function's output by interpolating in 3 dimension
        :param y:
            a query in y dimension to evaluate the function's output by interpolating in 3 dimension
        :param z:
            a query in z dimension to evaluate the function's output by interpolating in 3 dimension
        :return:
            interpolated values in 3 dimension
        """
        if np.isscalar(x):
            x = np.array([x])
        if np.isscalar(y):
            y = np.array([y])
        if np.isscalar(z):
            z = np.array([z])
        Nx = len(self.x) - 1
        Ny = len(self.y) - 1
        Nz = len(self.z) - 1
        f = np.zeros((len(x), len(y), len(z)))
        A = np.zeros((2, 2))
        B = np.zeros((2, 2))
        ii = 0
        for valx in x:
            # finding the last index which its value is smaller than val in x dimension
            i = np.floor(np.where(self.x <= valx)[0][-1]).astype(int)
            if (i == Nx):
                i -= 1
            jj = 0
            for valy in y:
                # finding the last index which its value is smaller than val in y dimension
                j = np.floor(np.where(self.y <= valy)[0][-1]).astype(int)
                if (j == Ny):
                    j -= 1
                kk = 0
                for valz in z:
                    # finding the last index which its value is smaller than val in z dimension
                    k = np.floor(np.where(self.z <= valz)[0][-1]).astype(int)
                    if (k == Nz):
                        k -= 1
                    # finding the interpolation ratio in x dimension
                    tx = (valx - self.x[i]) / self.hx[i]
                    # finding the interpolation ratio in y dimension
                    ty = (valy - self.y[j]) / self.hy[j]
                    # finding the interpolation ratio in z dimension
                    tz = (valz - self.z[k]) / self.hz[k]
                    # calculating the interpolated value in 2 dimensions by matrix multiplication equation 3 in
                    # second slides
                    ptx = np.array([l1(tx), l2(tx)])
                    pty = np.array([l1(ty), l2(ty)])
                    ptz = np.array([l1(tz), l2(tz)])
                    B[0, :] = np.array([self.f[i, j, k], self.f[i, j, k + 1]])
                    B[1, :] = np.array([self.f[i + 1, j, k], self.f[i + 1, j, k + 1]])
                    A[:, 0] = np.dot(B, ptz)
                    B[0, :] = np.array([self.f[i, j + 1, k], self.f[i, j + 1, k + 1]])
                    B[1, :] = np.array([self.f[i + 1, j + 1, k], self.f[i + 1, j + 1, k + 1]])
                    A[:, 1] = np.dot(B, ptz)
                    f[ii, jj, kk] = np.dot(ptx, np.dot(A, pty))
                    kk += 1
                jj += 1
            ii += 1
        return f
    # end eval3d


# end class linear interp


def main():
    fig1d = plt.figure()
    ax1d = fig1d.add_subplot(111)

    # 1d example
    x = np.linspace(0., 2. * np.pi, 10)
    y = np.sin(x)
    lin1d = linear_interp(x=x, f=y, dims=1)
    xx = np.linspace(0., 2. * np.pi, 100)
    inter_1d = lin1d.eval1d(xx)
    true_data = np.sin(xx)
    ax1d.plot(xx, inter_1d)
    ax1d.plot(x, y, 'o', xx, true_data, 'r--')
    ax1d.set_title('function')
    print('Error 1d linear interpolation', np.sum((true_data - inter_1d) ** 2) / len(xx))

    # 2d example
    fig2d = plt.figure()
    ax2d = fig2d.add_subplot(221, projection='3d')
    ax2d2 = fig2d.add_subplot(222, projection='3d')
    ax2d3 = fig2d.add_subplot(223)
    ax2d4 = fig2d.add_subplot(224)

    x = np.linspace(-2.0, 2.0, 11)
    y = np.linspace(-2.0, 2.0, 11)
    X, Y = np.meshgrid(x, y)
    Z = X * np.exp(-1.0 * (X * X + Y * Y))
    ax2d.plot_wireframe(X, Y, Z)
    ax2d3.pcolor(X, Y, Z)
    # ax2d3.contourf(X,Y,Z)

    lin2d = linear_interp(x=x, y=y, f=Z, dims=2)
    x = np.linspace(-2.0, 2.0, 51)
    y = np.linspace(-2.0, 2.0, 51)
    X, Y = np.meshgrid(x, y)
    inter_2d = lin2d.eval2d(x, y)
    true_data = X * np.exp(-1.0 * (X * X + Y * Y))
    print('Error 2d linear interpolation', np.sum((true_data - inter_2d) ** 2) / len(x))

    ax2d2.plot_wireframe(X, Y, inter_2d)
    ax2d4.pcolor(X, Y, inter_2d)

    # 3d example
    x = np.linspace(0.0, 3.0, 10)
    y = np.linspace(0.0, 3.0, 10)
    z = np.linspace(0.0, 3.0, 10)
    X, Y, Z = np.meshgrid(x, y, z)
    F = (X + Y + Z) * np.exp(-1.0 * (X * X + Y * Y + Z * Z))
    X, Y = np.meshgrid(x, y)
    fig3d = plt.figure()
    ax = fig3d.add_subplot(121)
    ax.pcolor(X, Y, F[..., int(len(z) / 2)])
    lin3d = linear_interp(x=x, y=y, z=z, f=F, dims=3)

    x = np.linspace(0.0, 3.0, 50)
    y = np.linspace(0.0, 3.0, 50)
    z = np.linspace(0.0, 3.0, 50)
    X, Y = np.meshgrid(x, y)
    X1, Y1, Z1 = np.meshgrid(x, y, z)
    inter_3d = lin3d.eval3d(x, y, z)
    true_data = (X1 + Y1 + Z1) * np.exp(-1.0 * (X1 * X1 + Y1 * Y1 + Z1 * Z1))
    print('Error 3d linear interpolation', np.sum((true_data - inter_3d) ** 2) / len(x))
    ax2 = fig3d.add_subplot(122)
    ax2.pcolor(X, Y, inter_3d[..., int(len(z) / 2)])

    plt.show()


# end main

if __name__ == "__main__":
    main()
