import numpy as np
import Integration as integral
import root_search
import multi_dimensional_gradient as grad
import steepest_descent
import linear_interp
import spline_class

if __name__ == '__main__':
    integral.integration()
    linear_interp.main()
    spline_class.main()
    interpolated_result = root_search.search()
    print('interpolation result from root search', interpolated_result)
    grad.test_gradient()
    steepest_descent.test_steepest_descent(10)
