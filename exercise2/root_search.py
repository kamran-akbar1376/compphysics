import numpy as np


def h_func(r_max=100, r_0=1e-5, dim=100):
    """

    :param r_max:
        maximum r
    :param r_0:
        initial r
    :param dim:
        grid dimensions
    :return:
        essential h for grid generation function
    """
    return np.log(r_max / r_0 + 1) / (dim - 1)


def generate_grid(h, dim, r_0, x_begin=0, x_end=1):
    """

    :param h:
        computed by h_func
    :param dim:
        grid dimensions
    :param r_0:
        initial r
    :param x_begin:
        beginning of the x grid
    :param x_end:
        ending of the x grid
    :return:
        x linear grid, r output computed from x
    """
    if x_begin > x_end:
        print('x_begin is larger than x_end')
        return
    x = np.linspace(x_begin, x_end, dim)
    r = r_0 * (np.exp(x * h) - 1)
    return r, x


def search(r_max=100, r_0=1e-5, dim=100, x_begin=0, x_end=1, search_pos=0.5):
    """

    :param r_max:
        maximum r
    :param r_0:
        initial r
    :param dim:
        grid dimensions
    :param x_begin:
        beginning of the x grid
    :param x_end:
        ending of the x grid
    :param search_pos:
        position between x_begin and x_end which its value is unknown and will be calculated
        by linear interpolation in grids
    :return:
    """
    if search_pos > x_end or search_pos < x_begin:
        print('search position must placed between x_begin and x_end')
        return
    h = h_func(r_max, r_0, dim)
    r, x = generate_grid(h, dim, r_0, x_begin, x_end)

    i_start = 0
    i_end = dim
    middle = (i_start + i_end) / 2
    search_index = 0
    # Bisection method is used for finding the desired index
    while True:
        index = int(middle)
        if x[index] < search_pos:
            i_start = middle
        if search_pos < x[index]:
            i_end = middle
        middle = (i_start + i_end) / 2
        if np.abs(i_start - i_end) < 1:
            search_index = int(i_start)
            break
    # compute the function's output at search position by linear interpolation between the index
    # resulted from bisection method, and that index + 1
    t = (search_pos - x[search_index]) / (x[search_index + 1] - x[search_index])
    return (1 - t) * r[search_index] + t * r[search_index + 1]
