import numpy as np


def func(x):
    """
    function for calculating the numerical gradient
    """
    if len(x) >= 3:
        return np.sin(x[0]) + np.cos(x[len(x) - 1]) + np.sum(x[1: len(x) - 1] ** 2)
    else:
        print('the input\'s length must be greater than or equal three.')
        return


def func_gradient(x):
    """
    real gradient computed analytically
    """
    grad = np.zeros(len(x))
    grad[0] = np.cos(x[0])
    grad[len(x) - 1] = -np.sin(x[len(x) - 1])
    for i in range(1, len(x) - 1):
        grad[i] = 2 * x[i]
    return grad


def gradient(f, x, dx):
    """

    :param f:
        function for computing gradient
    :param x:
        input for function
    :param dx:
        small number for delta x
    :return:
        computed gradient by partial derivative in each dimension
    """
    grad = np.zeros(len(x))
    for i in range(0, len(x)):
        x_plus = np.array(x)
        x_plus[i] += dx
        x_minus = np.array(x)
        x_minus[i] -= dx
        grad[i] = (f(x_plus) - f(x_minus)) / (2 * dx)
    return grad


def test_gradient():
    tol = 1e-4
    x = np.random.randint(1, 10, 5).astype(np.float)
    grad = gradient(func, x, 1e-3)
    true_grad = func_gradient(x)
    error = np.sum(np.sqrt((grad - true_grad) ** 2))
    if error < tol:
        print('Correct Gradient!')
        return True
    return False
