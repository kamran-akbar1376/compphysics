import numpy as np
import multi_dimensional_gradient as grad


def func(x):
    return np.sum(x ** 2)


def min_func(n):
    return np.zeros(n)


def steep_desc(f, init_x, dx):
    """

    :param f:
        function for finding its minimum
    :param init_x:
        initial guess for function's input in N dimensions
    :param dx:
        small number for delta x
    :return:
        the minimum of f function in N dimensions
    """
    tol = 1e-3
    a = 1e-4
    x = np.array(init_x)
    min_x = np.array(init_x)
    while True:
        prev_x = np.array(x)
        gradient = grad.gradient(f, x, dx)
        gama = a / (np.abs(gradient) + 1)
        x -= gama * gradient
        if np.sqrt(np.sum(gradient ** 2)) < tol:
            min_x = np.array(x)
            break
    return min_x


def test_steepest_descent(n):
    tol = 1e-3
    init_x = np.ones(n)
    min_x = steep_desc(func, init_x, 1e-3)
    true_min_x = min_func(n)
    error = np.sqrt(np.sum((min_x - true_min_x) ** 2))
    if error < tol:
        print('Correct steepest descent')
        return True
    return False
