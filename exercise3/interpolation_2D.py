import numpy as np
from linear_interp import linear_interp
import matplotlib.pyplot as plt


def func(x, y):
    """
    Two dimensional function used for interpolation
    :param x:
    First dimension input
    :param y:
    Second dimension input
    :return:
    Function's output
    """
    return (x + y) * np.exp(-np.sqrt(x**2 + y**2))


def grid_generation(f, x0, x1, y0, y1, nx, ny):
    """
    This Function is used for generating a 2d sparse grid
    :param f:
    Specific 2d function for creating the output of the 2d grid
    :param x0:
    Beginning of the first dimension interval
    :param x1:
    End of the first dimension interval
    :param y0:
    Beginning of the second dimension interval
    :param y1:
    End of the second dimension interval
    :param nx:
    Number of samples in the first dimension
    :param ny:
    Number of samples in the second dimension
    :return:
    Three arrays including linear space in x and y dimension, and their output resulted from
    function f
    """
    x = np.linspace(x0, x1, nx)
    y = np.linspace(y0, y1, ny)
    X, Y = np.meshgrid(x, y)
    Z = f(X, Y)
    return Z, x, y


def interpolation_2D_example():
    """
    This function generate a 30x30 grid; then, interpolate 100 values placed in the
    y = x^2 path by eval2D_path in the linear.interp.py
    """
    Z, x, y = grid_generation(func, -2, 2, -2, 2, 30, 30)
    X, Y = np.meshgrid(x, y)
    lin2 = linear_interp(x=x, y=y, f=Z, dims=2)
    xx = np.linspace(0, 1, 100)
    yy = xx ** 2
    interp_values = lin2.eval2D_path(xx, yy)
    true_value = func(xx, yy)
    plt.plot(yy, true_value, yy, interp_values, 'r--')
    plt.show()


