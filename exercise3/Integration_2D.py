import numpy as np
from scipy.integrate import simps


def func(x, y):
    """
    Function for testing the numerical double integration
    :param x:
    first dimension input
    :param y:
    second dimension input
    :return:
    output in 2 dimensional matrix computed for each x
    """
    return (x[:, None] + y) ** 2 * np.exp(-np.sqrt(x[:, None] ** 2 + y ** 2))


def integration_2D(f, x, y):
    """
    A Function for calculating double integration numerically. The numerical method is simpson
    :param f:
    The function which is integrated
    :param x:
    input for first dimension
    :param y:
    input for second dimension
    :return:
    double integration of f function according to x and y dimensions
    """

    z = func(x, y)
    return simps(simps(z, x), y)


def test_integration_2D():
    """
    Function for testing the accuracy of double integration which the accuracy is computed regarding to
    Matlab integral2 output. The testing happen with three integration grid.
    """
    error = np.zeros(5)
    y = np.linspace(-2, 2, 1000)
    x = np.linspace(0, 2, 1000)
    true_integral = 3.6029
    integral = integration_2D(func, x, y)
    error[0] = np.abs(true_integral - integral)

    x = np.linspace(3, 10, 1000)
    true_integral = 3.1159
    integral = integration_2D(func, x, y)
    error[1] = np.abs(true_integral - integral)

    x = np.linspace(13, 20, 1000)
    true_integral = 0.0017
    integral = integration_2D(func, x, y)
    error[2] = np.abs(true_integral - integral)

    print("Double Integration Error", np.sum(error ** 2))
