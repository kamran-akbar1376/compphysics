"""
We can call other scripts from the main.py
"""
import Integration_2D as integral
import interpolation_2D as interpolation
import matrix_eigs as eig
import electric_field

if __name__ == '__main__':
    integral.test_integration_2D()
    interpolation.interpolation_2D_example()
    eig.test_eigen()
    electric_field.visualize_e_field()
