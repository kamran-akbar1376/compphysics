"""
Linear interpolation in 1d, 2d, and 3d

Intentionally unfinished :)

Related to FYS-4096 Computational Physics
exercise 2 assignments.

By Ilkka Kylanpaa on January 2019
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

"""
Add basis functions l1 and l2 here
"""


def l1(t):
    return 1 - t


def l2(t):
    return t


class linear_interp:
    """
        linear_interp class is for linear interpolation in 1 to 3 dimensions
    """

    def __init__(self, *args, **kwargs):
        """
        This constructor define the interpolation's dimension, and get each dimensions data through kwargs, and
        calculate grid space between each dimension's input.
        """
        self.dims = kwargs['dims']
        if (self.dims == 1):
            self.x = kwargs['x']
            self.f = kwargs['f']
            self.hx = np.diff(self.x)
        elif (self.dims == 2):
            self.x = kwargs['x']
            self.y = kwargs['y']
            self.f = kwargs['f']
            self.hx = np.diff(self.x)
            self.hy = np.diff(self.y)
        elif (self.dims == 3):
            self.x = kwargs['x']
            self.y = kwargs['y']
            self.z = kwargs['z']
            self.f = kwargs['f']
            self.hx = np.diff(self.x)
            self.hy = np.diff(self.y)
            self.hz = np.diff(self.z)
        else:
            print('Either dims is missing or specific dims is not available')

    def eval1d(self, x):
        """
        calculating the interpolation values in 1 dimension
        :param x:
            a query to evaluate the function's output by interpolating in 1 dimension
        :return:
            interpolated values in 1 dimension
        """
        if np.isscalar(x):
            x = np.array([x])
        N = len(self.x) - 1
        f = np.zeros((len(x),))
        ii = 0
        for val in x:
            # finding the last index which its value is smaller than val
            i = np.floor(np.where(self.x <= val)[0][-1]).astype(int)
            # interpolation process with boundary condition
            if i == N:
                f[ii] = self.f[i]
            else:
                # finding the interpolation ratio
                t = (val - self.x[i]) / self.hx[i]
                # calculate interpolated output equation 1 in second slides
                f[ii] = self.f[i] * l1(t) + self.f[i + 1] * l2(t)
            ii += 1
        return f

    def eval2d(self, x, y):
        """
        calculating the interpolation values in 2 dimension
        :param x:
            a query in x dimension to evaluate the function's output by interpolating in 2 dimension
        :param y:
            a query in y dimension to evaluate the function's output by interpolating in 2 dimension
        :return:
            interpolated values in 2 dimension
        """
        if np.isscalar(x):
            x = np.array([x])
        if np.isscalar(y):
            y = np.array([y])
        Nx = len(self.x) - 1
        Ny = len(self.y) - 1
        f = np.zeros((len(x), len(y)))
        A = np.zeros((2, 2))
        ii = 0
        for valx in x:
            # finding the last index which its value is smaller than val in x dimension
            i = np.floor(np.where(self.x <= valx)[0][-1]).astype(int)
            if (i == Nx):
                i -= 1
            jj = 0
            for valy in y:
                # finding the last index which its value is smaller than val in y dimension
                j = np.floor(np.where(self.y <= valy)[0][-1]).astype(int)
                if (j == Ny):
                    j -= 1
                # finding the interpolation ratio in x dimension
                tx = (valx - self.x[i]) / self.hx[i]
                # finding the interpolation ratio in y dimension
                ty = (valy - self.y[j]) / self.hy[j]
                # calculating the interpolated value in 2 dimensions by matrix multiplication equation 2 in second
                # slides
                ptx = np.array([l1(tx), l2(tx)])
                pty = np.array([l1(ty), l2(ty)])
                A[0, :] = np.array([self.f[i, j], self.f[i, j + 1]])
                A[1, :] = np.array([self.f[i + 1, j], self.f[i + 1, j + 1]])
                f[ii, jj] = np.dot(ptx, np.dot(A, pty))
                jj += 1
            ii += 1
        return

    # end eval2d

    def eval2D_path(self, x, y):
        """
        This function computes 2D interpolation in a specific path
        :param x:
        input for the first dimension
        :param y:
        input for the second dimension
        :return:
        interpolated values in the x, y path which means for each x[i] and y[i] there is one f[i] interpolated value
        """
        if np.isscalar(x):
            x = np.array([x])
        if np.isscalar(y):
            y = np.array([y])
        Nx = len(self.x) - 1
        Ny = len(self.y) - 1
        f = np.zeros(len(x))
        A = np.zeros((2, 2))
        ii = 0
        """
        This loop is for computing interpolated value for each x[i] and y[i] which stored in the f[i]
        """
        for i in range(0, len(x)):
            index1 = np.floor(np.where(self.x <= x[i])[0][-1]).astype(int)
            index2 = np.floor(np.where(self.y <= y[i])[0][-1]).astype(int)
            if index1 == Nx:
                index1 -= 1
            if index2 == Ny:
                index2 -= 1
            tx = (x[i] - self.x[index1]) / self.hx[index1]
            ty = (y[i] - self.y[index2]) / self.hy[index2]
            ptx = np.array([l1(tx), l2(tx)])
            pty = np.array([l1(ty), l2(ty)])
            A[0, :] = np.array([self.f[index1, index2], self.f[index1, index2 + 1]])
            A[1, :] = np.array([self.f[index1 + 1, index2], self.f[index1 + 1, index2 + 1]])
            f[i] = np.dot(ptx, np.dot(A, pty))
        return f

    def eval3d(self, x, y, z):
        """
        calculating the interpolation values in 3 dimension
        :param x:
            a query in x dimension to evaluate the function's output by interpolating in 3 dimension
        :param y:
            a query in y dimension to evaluate the function's output by interpolating in 3 dimension
        :param z:
            a query in z dimension to evaluate the function's output by interpolating in 3 dimension
        :return:
            interpolated values in 3 dimension
        """
        if np.isscalar(x):
            x = np.array([x])
        if np.isscalar(y):
            y = np.array([y])
        if np.isscalar(z):
            z = np.array([z])
        Nx = len(self.x) - 1
        Ny = len(self.y) - 1
        Nz = len(self.z) - 1
        f = np.zeros((len(x), len(y), len(z)))
        A = np.zeros((2, 2))
        B = np.zeros((2, 2))
        ii = 0
        for valx in x:
            # finding the last index which its value is smaller than val in x dimension
            i = np.floor(np.where(self.x <= valx)[0][-1]).astype(int)
            if (i == Nx):
                i -= 1
            jj = 0
            for valy in y:
                # finding the last index which its value is smaller than val in y dimension
                j = np.floor(np.where(self.y <= valy)[0][-1]).astype(int)
                if (j == Ny):
                    j -= 1
                kk = 0
                for valz in z:
                    # finding the last index which its value is smaller than val in z dimension
                    k = np.floor(np.where(self.z <= valz)[0][-1]).astype(int)
                    if (k == Nz):
                        k -= 1
                    # finding the interpolation ratio in x dimension
                    tx = (valx - self.x[i]) / self.hx[i]
                    # finding the interpolation ratio in y dimension
                    ty = (valy - self.y[j]) / self.hy[j]
                    # finding the interpolation ratio in z dimension
                    tz = (valz - self.z[k]) / self.hz[k]
                    # calculating the interpolated value in 2 dimensions by matrix multiplication equation 3 in
                    # second slides
                    ptx = np.array([l1(tx), l2(tx)])
                    pty = np.array([l1(ty), l2(ty)])
                    ptz = np.array([l1(tz), l2(tz)])
                    B[0, :] = np.array([self.f[i, j, k], self.f[i, j, k + 1]])
                    B[1, :] = np.array([self.f[i + 1, j, k], self.f[i + 1, j, k + 1]])
                    A[:, 0] = np.dot(B, ptz)
                    B[0, :] = np.array([self.f[i, j + 1, k], self.f[i, j + 1, k + 1]])
                    B[1, :] = np.array([self.f[i + 1, j + 1, k], self.f[i + 1, j + 1, k + 1]])
                    A[:, 1] = np.dot(B, ptz)
                    f[ii, jj, kk] = np.dot(ptx, np.dot(A, pty))
                    kk += 1
                jj += 1
            ii += 1
        return f
    # end eval3d


# end class linear interp
