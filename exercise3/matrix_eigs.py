"""
FYS-4096 Computational physics 

1. Add code to function 'largest_eig'
- use the power method to obtain 
  the largest eigenvalue and the 
  corresponding eigenvector of the
  provided matrix

2. Compare the results with scipy's eigs
- this is provided, but you should use
  that to validating your power method
  implementation

Hint: 
  dot(A,x), A.dot(x), A @ x could be helpful for 
  performing matrix operations

"""

from numpy import *
from matplotlib.pyplot import *
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps


def largest_eig(A, tol=1e-12):
    """
    Function that compute maximum eigen value and eigen vector of a matrix numerically
    :param A:
    A nxn dimensional matrix which its eigen value and eigen vector would be computed
    :param tol:
    a threshold value to make the iterative algorithm stop
    :return:
    maximum eigen value and corresponding eigen vector of matrix A
    """

    # initial guess of eigen vector with only first element as one
    init_guess = np.zeros(A.shape[0])
    init_guess[0] = 1
    eigen_value = 0
    eigen_vector = np.zeros(len(init_guess))
    x = init_guess
    n_k = max(abs(x))
    """
    Iterative algorithm is: 
    p = A * x_k
    n_k = max(|p|)
    x_k_plus_one = p / n_k
    
    The stop condition is:
    |n_k - prev_n_k| < tol
    when the algorithm reached to that level:
    maximum eigen value is n_k
    corresponding eigen vector is x_k
    """
    while True:
        p = np.dot(A, x)
        prev_n_k = n_k
        n_k = max(abs(p))
        x = np.array(p / n_k)
        if abs(n_k - prev_n_k) < tol:
            eigen_value = n_k
            eigen_vector = x / np.linalg.norm(x)
            break
    return eigen_value, eigen_vector


def test_eigen():
    grid = linspace(-5, 5, 100)
    grid_size = grid.shape[0]
    dx = grid[1] - grid[0]
    dx2 = dx * dx

    # make test matrix
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size) - 1.0 / (abs(grid) + 0.8),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1]).toarray()

    # use scipy to calculate the largest eigenvalue
    # and corresponding vector
    eigs, evecs = sla.eigsh(H0, k=1, which='LA')

    # use your power method to calculate the same
    l, vec = largest_eig(H0)

    # see how they compare
    print('largest_eig estimate: ', l)
    print('scipy eigsh estimate: ', eigs)

    # eigsh eigen vector
    psi0 = evecs[:, 0]
    norm_const = simps(abs(psi0) ** 2, x=grid)
    psi0 = psi0 / norm_const

    # largest_eig eigen vector 
    psi0_ = vec
    norm_const = simps(abs(psi0_) ** 2, x=grid)
    psi0_ = psi0_ / norm_const

    plot(grid, abs(psi0) ** 2, label='scipy eig. vector squared')
    plot(grid, abs(psi0_) ** 2, 'r--', label='largest_eig vector squared')
    legend(loc=0)
    show()
