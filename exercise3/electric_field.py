import numpy as np
from scipy.integrate import simps
import matplotlib.pyplot as plt


def e_field(x, y, q=(4 * np.pi * 8.85e-12) ** (-1), l=2):
    """

    :param x:
    arbitrary position x in space
    :param y:
    arbitrary position y in space
    :param q:
    rod electrical charge which is equal (4 * np.pi * 8.85e-12) ** (-1)
    :param l:
    rod length
    :return:
    electrical field in both y and x dimension
    """

    constant = (4 * np.pi * 8.85e-12) ** (-1)
    if y != 0:
        def dEx(x_rod):
            return (q / (constant * l)) * (np.sqrt(y ** 2 + (x - x_rod) ** 2)) ** (-2) * np.sin(
                np.arctan2((x - x_rod), y))

        def dEy(x_rod):
            return (q / (constant * l)) * (np.sqrt(y ** 2 + (x - x_rod) ** 2)) ** (-2) * np.cos(
                np.arctan2((x - x_rod), y))

        xx = np.linspace(-l / 2, l / 2, 1000)
        zz = dEy(xx)
        Ey = simps(zz, xx)
    else:
        def dEx(x_rod):
            return (q / (constant * l)) * (np.sqrt(y ** 2 + (x - x_rod) ** 2)) ** (-2)

        Ey = 0

    xx = np.linspace(-l / 2, l / 2, 1000)
    zz = dEx(xx)
    Ex = simps(zz, xx)
    return Ex, Ey


def true_e_field(x, q=(4 * np.pi * 8.85e-12) ** (-1), l=2):
    """

    :param x:
    arbitrary position x in space
    :param q:
    rod electrical charge which is equal (4 * np.pi * 8.85e-12) ** (-1)
    :param l:
    rod length
    :return:
    electrical field computed analytically according to x direction in y = 0
    """
    constant = (4 * np.pi * 8.85e-12) ** (-1)
    return (q / (l * constant)) * ((1 / (x - l / 2)) - (1 / (x + l / 2))), 0


def visualize_e_field():
    # check the computed numerical electric field versus analytical electric field
    print('Electric field numerical:', e_field(1.1, 0))
    print('Electric field analytical:', true_e_field(1.1))

    X, Y = np.meshgrid(np.linspace(-3, 3, 20), np.linspace(-3, 3, 20))

    Ex = np.zeros(X.shape)
    Ey = np.zeros(Y.shape)

    # compute electric field in each position inside the xy plane
    for i in range(0, 20):
        for j in range(0, 20):
            Ex[i, j], Ey[i, j] = e_field(X[i, j], Y[i, j])

    # visualize the electric field with quiver
    fig, ax = plt.subplots()
    ax.set_title("Electric field")
    M = np.hypot(Ex, Ey)
    Q = ax.quiver(X, Y, Ex, Ey, M, units='x', width=0.011)
    qk = ax.quiverkey(Q, 0.9, 0.9, 1, r'$1$', labelpos='E', coordinates='figure')
    ax.scatter(X, Y, color='0.5', s=1)

    plt.show()
