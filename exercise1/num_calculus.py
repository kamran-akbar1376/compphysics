import numpy as np

def first_derivative(function, x, dx):
    return (function(x + dx) - function(x - dx)) / (2 * dx)


def second_derivative(function, x, dx):
    return (function(x + dx) + function(x - dx) - 2 * function(x)) / dx ** 2


def simpson_integ(function, X, dx):
    N = len(X)
    integ = 0
    for i in range(0, int(N / 2) - 1):
        integ += function(X[2 * i]) + 4 * function(X[2 * i + 1]) + function(X[2 * i + 2])
    integ *= dx / 3
    if N % 2 == 1:
        integ += dx / 12 * (-function(X[N - 3]) + 8 * function(X[N - 2]) + 5 * function(X[N - 1]))
    return integ


def trapzoid_integ(function, X):
    N = len(X)
    dx = np.diff(X)
    integ = 0
    for i in range(0, N - 1):
        integ += (function(X[i]) + function(X[i + 1])) * dx[i];
    return integ * 0.5


# fun is the function meant for integration
# xmin is the beginning of the integration interval
# xmax is the end of the integration interval
# iters is the number of generating random numbers for only one round
# blocks shows how many integral calculated for the function fun
def monte_carlo_integration(fun, xmin, xmax, blocks=10, iters=100):
    # integral values started with zeros
    block_values = np.zeros((blocks,))
    # calculate the interval range
    L = xmax - xmin
    for block in range(blocks):
        for i in range(iters):
            # calculate a random x between the xmin and xmax
            x = xmin + np.random.rand() * L
            # calculate the out of the function with random generated xs and sum them up
            block_values[block] += fun(x)
        # calculate the average of each block values
        block_values[block] /= iters
    # calculate the final Integral by finding the mean of block values and multiplying it by range between xmin and xmax
    I = L * np.mean(block_values)
    # calculate the delta of the integration by calculating the standard derivation multiplied by the range between xmin and xmax which is V
    dI = L * np.std(block_values) / np.sqrt(blocks)
    return I, dI


def test_first_derivative(x, dx):
    tolerance = 10 * dx
    der = first_derivative(func, x, dx)
    true_der = derivative_func(x)
    if abs(der - true_der) < tolerance:
        print("True derivative")
        return True
    else:
        print("False derivative")
        return False


def test_second_derivative(x, dx):
    tolerance = 10 * dx
    der2 = second_derivative(func, x, dx)
    true_der2 = second_derivative_func(x)
    if abs(der2 - true_der2) < tolerance:
        print("True second derivative")
        return True
    else:
        print("False second derivative")
        return False


def test_simpson_integ(X):
    tolerance = 1.0e-3
    integ = simpson_integ(func, X, np.diff(X)[0])
    true_integ = integrate_func(X[0], X[len(X) - 1])
    if abs(integ - true_integ) < tolerance:
        print("True Integral Simpson")
        return True
    else:
        print("False Integral Simpson")
        return False


def test_trapzoid_integ(X):
    tolerance = 1.0e-3
    integ = trapzoid_integ(func, X)
    true_integ = integrate_func(X[0], X[len(X) - 1])
    if abs(integ - true_integ) < tolerance:
        print("True Integral Trapzoid")
        return True
    else:
        print("False Integral Trapzoid")
        return False


def test_monte_carlo_integration(begin, end, blocks=10, iters=100):
    tolerance = 1.0e-3
    I, dI = monte_carlo_integration(func, begin, end, blocks, iters)
    true_I = integrate_func(begin, end)
    if abs(I + dI - true_I) < tolerance or abs(I - dI - true_I) < tolerance:
        print("True Integral Monte Carlo")
        return True
    else:
        print("False Integral Monte Carlo")
        return False


def func(x):
    return np.sin(x)


def derivative_func(x):
    return np.cos(x)


def second_derivative_func(x):
    return -np.sin(x)


def integrate_func(begin, end):
    return -np.cos(end) + np.cos(begin)
