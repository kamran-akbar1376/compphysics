import num_calculus as nmcalc
import convergence_check as conv
import numpy as np

if __name__ == '__main__':
    nmcalc.test_first_derivative(0.5, 0.001)
    nmcalc.test_second_derivative(0.5, 0.001)
    X = np.linspace(0, np.pi, 300)
    nmcalc.test_trapzoid_integ(X)
    nmcalc.test_simpson_integ(X)
    nmcalc.test_monte_carlo_integration(0., np.pi / 2, 100, 1000)
    conv.plot_derivative_error()


