import numpy as np
import matplotlib.pyplot as plt
from num_calculus import first_derivative, simpson_integ


def func(x):
    return np.cos(x)


def derivative_func(x):
    return -np.sin(x)


def second_derivative_func(x):
    return np.sin(x)


def integrate_func(begin, end):
    return np.sin(end) - np.sin(begin)


def plot_derivative_error():
    dx = np.array([0.00001, 0.0001, 0.001, 0.01, 0.1, 1])
    derivative = first_derivative(func, 0.5, dx)
    true_derivative = derivative_func(0.5)
    abs_error_der = abs(derivative - true_derivative)

    abs_error_integral = np.zeros(6)
    count = 0
    for i in dx:
        X = np.linspace(0, 10000 * i, 10000)
        true_integral = integrate_func(X[0], X[len(X) - 1])
        integral = simpson_integ(func, X, i)
        abs_error_integral[count] = abs(integral - true_integral)
        count += 1

    plt.figure(figsize=(9, 5))

    plt.subplot(121)
    plt.xlabel('dx')
    plt.ylabel('first derivative')
    plt.title('First derivative estimate error')
    plt.plot(dx, abs_error_der)
    plt.subplot(122)
    plt.xlabel('dx')
    plt.ylabel('integral')
    plt.title('Simpson integral estimate error')
    plt.plot(dx, abs_error_integral)
    plt.suptitle('Numerical Estimate Error')
    plt.show()
